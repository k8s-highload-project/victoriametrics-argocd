# Ручная установка

## 1. Эскпортёры.

### 1.1 Kube-state-metrics

[Документация](https://github.com/kubernetes/kube-state-metrics).

Предоставляет основную информацию о кластере kubernetes. Данные берёт из запросов к API kubernetes.
[Helm chart github](https://github.com/prometheus-community/helm-charts)
 
    helm search repo prometheus-community
    helm show chart prometheus-community/kube-state-metrics
    helm show values prometheus-community/kube-state-metrics > values.yaml

    helm repo add prometheus-community https://prometheus-community.github.io/helm-charts
    helm repo update
    helm install kube-state-metrics charts/ksm

[Application kube-state-metrics для ArgoCD](applications/01-ksm.yaml)

### 1.2 Node exporter

[Документация](https://github.com/prometheus/node_exporter).

Информация о Linux машинах.
[Helm chart github](https://github.com/prometheus-community/helm-charts)

    helm search repo prometheus-community
    helm show chart prometheus-community/prometheus-node-exporter
    helm show values prometheus-community/prometheus-node-exporter > values.yaml

    helm repo add prometheus-community https://prometheus-community.github.io/helm-charts
    helm repo update
    helm install node-exporter charts/nexporter

[Application prometheus-node-exporter для ArgoCD](applications/02-nexporter.yaml)

### 1.3 Gitlab-ci-pipelines-exporter

Мониторинг состояния Gitlab пайплайнов, job-ов, переменные окружения, статусы, продолжительность и т.д.

Автоматизация [Application Gitlab-ci-pipelines-exporter для ArgoCD](applications/09-ci-pipelines-exporter.yaml)


[Репозиторий github](https://github.com/mvisonneau/gitlab-ci-pipelines-exporter)

    helm repo add mvisonneau https://charts.visonneau.fr
    helm repo update

    helm show chart mvisonneau/gitlab-ci-pipelines-exporter

    helm show values mvisonneau/gitlab-ci-pipelines-exporter > values.yaml

    helm install gitlab-ci-pipelines-exporter mvisonneau/gitlab-ci-pipelines-exporter -f values.yaml -n monitoring --create-namespace

    helm uninstall gitlab-ci-pipelines-exporter -n monitoring


Добавить для vmagent в values.yaml

```
  extraScrapeConfigs:
    - job_name: 'gitlab-ci-pipelines-exporter'
      static_configs:
        - targets: [ 'gitlab-ci-pipelines-exporter.monitoring.svc.cluster.local:8080' ]
```

Для grafana plugin [grafana-polystat-panel](https://grafana.com/grafana/plugins/grafana-polystat-panel/?tab=installation)

[Helm chart github](https://github.com/mvisonneau/helm-charts)

You can check the chart's [values.yml](https://github.com/mvisonneau/helm-charts/blob/main/charts/gitlab-ci-pipelines-exporter/values.yaml) for complete configuration options.

The **configuration syntax** [is maintained here](https://github.com/mvisonneau/gitlab-ci-pipelines-exporter/blob/main/docs/configuration_syntax.md).


**Загрузка непосредственно из чарта.**

    helm pull mvisonneau/gitlab-ci-pipelines-exporter --untar

[Статья на хабре](https://habr.com/ru/articles/646255/)

[Видео инструкция](https://youtu.be/cc833YuhI5s)

**Создание секрета для токены gitlab**

    echo -n 'glpat-NczomxuQbB2dcaL5QEx9' | base64

    apiVersion: v1
    kind: Secret
    metadata:
      name: gitlab-token-secret
      namespace: monitoring
    type: Opaque
    data:
      gitlabToken: Z2xwYXQtTmN6b214dVFiQjJkY2FMNVFFeDk=

    kubectl apply -f gitlabtoken.yaml

### 1.4 Docker compose prometheus exporter для vm вне кластера

[Документация Docker compose prometheus exporter](https://github.com/prometheus/node_exporter).

## 2. База данных, агент, grafana

### 2.1 Victoriametrics используем только в качестве хранилища
[Helm chart github](https://github.com/VictoriaMetrics/helm-charts/)

    helm search repo vm
    helm show chart vm/victoria-metrics-single
    helm show values vm/victoria-metrics-single > values.yaml

    helm repo add vm https://victoriametrics.github.io/helm-charts/
    helm repo update
    helm install victoriametrics charts/vm -f charts/vm/my-values.yaml -n monitoring

[Application victoriametrics для ArgoCD](applications/03-victoriametrics.yaml)

### 2.2 Vmagent
[Helm chart github](https://github.com/VictoriaMetrics/helm-charts/)

    helm search repo vm
    helm show chart vm/victoria-metrics-agent
    helm show values vm/victoria-metrics-agent > values.yaml

    helm repo add vm https://victoriametrics.github.io/helm-charts/
    helm repo update
    helm install vmagent charts/vmagent -f charts/vmagent/my-values.yaml -n monitoring

[Application vmagent для ArgoCD](applications/04-vmagent.yaml)

### 2.3 Grafana

Пришлось отказаться от ingress из стандартного чарта. Он не поддерживает генерацию ingressClassName, только
соответствующую аннотацию.

Что бы не "лезть руками" в чарт grafana, добавил генерацию своего ingress. Это конечно костыли, но они работают.

[Helm chart github](https://github.com/grafana/helm-charts)

    helm search repo grafana
    helm show chart grafana/grafana
    helm show values grafana/grafana > values.yaml
 
    helm repo add grafana https://grafana.github.io/helm-charts
    helm repo update
    helm install grafana charts/grafana -f charts/grafana/my-values.yaml -n monitoring

    Логин и пароль
    kubectl -n monitoring get secret grafana -o jsonpath='{.data.admin-password}' | base64 -d
    kubectl -n monitoring get secret grafana -o jsonpath='{.data.admin-user}' | base64 -d

[Application grafana для ArgoCD](applications/05-grafana.yaml)

## 3. Приложения обрабатывающие алёрты.

### 3.1 Alertmanager

[Helm chart github](https://github.com/prometheus-community/helm-charts/tree/main/charts/alertmanager)

    helm search repo prometheus-community/alertmanager
    helm show chart prometheus-community/alertmanager
    helm show values prometheus-community/alertmanager > values.yaml

    helm repo add prometheus-community https://prometheus-community.github.io/helm-charts
    helm repo update
    helm install alertmanager charts/alertmanager -f charts/alertmanager/my-values.yaml -n monitoring

[Application alertmanager для ArgoCD](applications/06-alertmanager.yaml)

### 3.2 Vmalert

[Helm chart github](https://github.com/VictoriaMetrics/helm-charts/tree/master/charts/victoria-metrics-alert)

    Этапы сборки из chart vmalert в отдельные манифесты для удобства управления:
    
    1) cd charts/vmalert и создать mkdir charts
    2) cd charts/vmalert/charts
    3) helm pull vm/victoria-metrics-alert --untar
    4) cd manifests/vmalert
    5) helm template vmalert ../../charts/vmalert/ -f ../../charts/vmalert/my-values.yaml > vmalert-dep.yaml
    6) rm -r charts/vmalert/charts
    7) в файле manifest/vmalert/vmalert-dep.yaml удаляем строку namespace: default
    8) из файл vmaler-dep.yaml переносим Config в manifest/vmalert/cm-alert-cpu.yaml
    9) из файл vmaler-dep.yaml переносим все кроме Deployment в manifest/vmalert/add.yaml
    10) в файле add.yaml для kind: ClusterRoleBinding для subjects добавляем namespace: monitoring
    subjects:
      - kind: ServiceAccount
          name: vmalert-victoria-metrics-alert
          namespace: monitoring

    helm search repo vm
    helm show chart vm/victoria-metrics-alert
    helm show values vm/victoria-metrics-alert > values.yaml

    helm repo add vm https://victoriametrics.github.io/helm-charts/
    helm repo update
    helm install vmalert charts/vmalert -f charts/vmalert/my-values.yaml -n monitoring

[Application vmalert для ArgoCD](applications/07-vmalert.yaml)

Запускаем с одним настроенным алёртом. Иначе не запустится. Более точно будем настраивать позднее.

### 3.3 Karma

[Helm chart github](https://github.com/wiremind/wiremind-helm-charts/tree/main/charts/karma)

    helm search repo wiremind/karma
    helm show chart wiremind/karma
    helm show values wiremind/karma > values.yaml

    helm repo add wiremind https://wiremind.github.io/wiremind-helm-charts
    helm repo update
    helm install karma charts/karma -f charts/karma/my-values.yaml -n monitoring

[Application karma для argocd](applications/08-karma.yaml)

Базовые настройки, позднее будем тюнить.