# Настройка Grafana

## 1. Корректное создание invite пользователей

Добавим пару конфигурационных параметров, что бы графана генерировала правильные url при созданнии invite для пользователей.

В my-values добавляем объект:

```yaml
  grafana.ini:
    server:
      domain: grafana.darveter.com
      root_url: https://grafana.darveter.com
```

## 2. Автоматическое добавление dashboards

После установки grafana мы можем добавлять в неё новые дашборды. Но если система будет разворачиваться "с нуля", мы каждый раз будем получать пустую grafana. И каждый раз будем руками добавлять новые dashboards.

Посмотрим, как это вопрос можно автоматизировать. 

Самый простой (по трудозатратам) dashboard на котором мы можем поэкспериментировать - это 
[dashboard node-exporter](https://grafana.com/grafana/dashboards/1860).

## 3. Загрузка дашборда с сайта grafana.com

В файле my-values добавляем провайдера:

```yaml
  dashboardProviders:
    dashboardproviders.yaml:
      apiVersion: 1
      providers:
      - name: 'default'
        orgId: 1
        folder: ''
        type: file
        disableDeletion: true
        editable: false
        options:
          path: /var/lib/grafana/dashboards/default
```

Там же добавляем dashboard node-exporter по его Id на сайте https://grafana.com/grafana/dashboards

```yaml
  dashboards:
    default:
      node-exporter:
        gnetId: 1860
        revision: 36 # тут не забываем менять на последнюю версию
        datasource: Victoria
```

## 4. Загрузка дашборда из github формат json

Создадим в директории dashboards файл node-exporter.json. Поместим в него исходник и изменим в
нём поле uid на `"uid": "art-node-exporter"`. Явно указав uid dashboard.
Искать по ключу timezone должен найти "timezone": "browser" ниже будет "uid": "rYdddlPWk", меняем на "uid": "art-node-exporter"

[Исходник node-exporter-full.json](https://github.com/rfmoz/grafana-dashboards).

Заодно сразу укажем источник данных:

Искать по ключу annotations должен найти "annotations", находим поле datasource и меняем "uid": "Victoria"

```json
    "annotations": {
      "list": [
        {
          "$$hashKey": "object:1058",
          "builtIn": 1,
          "datasource": {
            "type": "datasource",
            "uid": "Victoria"
          },
```

Теперь файл шаблона будет доступен в git. Соответственно надо поменять
параметры загрузки шаблона в my-values:

```yaml
 dashboards:
    default:
      node-exporter:
        url: 'https://gitlab.com/k8s-highload-project/victoriametrics-argocd/-/raw/main/dashboards/node-exporter.json'
```

## 5. Загрузка дашборда непосредственно из чарта.

Самый удобный вариант поместить файл дашборда прямо внутри чарта. Поскольку мы используем чарт обёртку, нам
придётся сделать несколько дополнительных дйествий.

Создадим внутри текущего чарта директорию charts. Перейдём в неё.

    helm pull grafana/grafana --untar

В появившемся subchart, в директорию dashboards добавляем node-exporter.json файл с dashboard.

В my-values меняем источник на:

```yaml
  dashboards:
    default:
      node-exporter:
        file: dashboards/node-exporter.json'
```

## 6. Загрузка через манифесты (пока не понял как, сложный процес сжатия)

6.1 Этапы сборки из chart grafana в отдельные манифесты для удобства управления шаблонами:
    
1) cd charts/grafana и создать mkdir charts
2) cd charts/grafana/charts
3) helm pull grafana/grafana --untar
4) mkdircd manifests/grafana
5) helm template grafana ../../charts/grafana/ -f ../../charts/grafana/03-my-values.yaml > app.yaml


## Видео Артура Крюкова

[<img src="https://img.youtube.com/vi/kJpfUTRP2L8/maxresdefault.jpg" width="50%">](https://youtu.be/kJpfUTRP2L8)