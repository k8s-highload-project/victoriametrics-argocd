# Мониторинг

## Компоненты мониторинга.

![](images/image1.jpg)

* Exporters - разнообразные источники метрик в формате Prometheus. Метрики может отдавать непосредственно приложение, 
так и специально написанные для приложений экспортёры.
* [victoriametrics](https://docs.victoriametrics.com/Single-server-VictoriaMetrics.html) - быстрая и масштабируемая СУБД для хранения и обработки метрик, оптимизирована под скорость и занимает минимальный объем данных на дисках. Используем вариант victoriametrics single - один экземпляр приложения. Предназначен для небольших объемов и сроков хранения данных.
хранения данных.
* [vmagent](https://docs.victoriametrics.com/vmagent.html) - приложение, собирающее метрики из экспортёров и помещающее их в хранилище метрик. Имеет возможность работать не только с экспортёрами метрик Prometheus, но и с другими системами (Graphite, InfluxDB agent и т.д.). Может быть запущено несколько vmagent.
* [vmalert](https://docs.victoriametrics.com/vmalert.html) - Формирует алёрты на основании метрик, хранящихся в базе victoriametrics. Сгенерированные алёрты отправляет в alertmanager для дальнейшей обработки.
* [alertmanager](https://prometheus.io/docs/alerting/latest/alertmanager/) - обрабатывает алёрты, отправляемые клиентскими приложениями. Он заботится о дедупликации, группировке и маршрутизации алёртов к получателям. Также может выключать и запрещать алёрты.
* [karma](https://github.com/wiremind/wiremind-helm-charts/tree/main/charts/karma) - Приложение, формирующее дашборды на основании информации из alertmanager. Отображает текущие (не закрытые) алёрты. Позволяет гибко фильтровать информацию, основываясь на метках.
* [grafana](https://grafana.com/grafana/) - Приложение для создания дашбордов на основании различных источников данных. В том числе находящихся в victoriametrics.


# Варианты базовой установки:

## 1. Автоустановка через argocd applications

    kubectl -n argocd apply -f applications

## 2. Ручная базовая установка

### 2.1 [Ручная установка для k8s](help/hand-base-install.md)
* Exporters:
  1. Kube-state-metrics
  2. Node exporter
  3. Gitlab-ci-pipelines-exporter
  4. Docker compose prometheus exporter для vm вне кластера
* БД Victoriametrics
* Сборщик метрик Vmagent
* Визуализация графиков Grafana
* Приложения обрабатывающие алёрты Alertmanager, Vmalert, Karma

## 3. Виды метрик и настройка сервисов

### 3.1 Настройка метрик

Примеры настройки [Metrics](help/metrics.md)

### 3.2 Настройка Grafana

Настраиваем [Grafana](help/grafana.md)

## 4. Настройка Vmalert

Настраиваем [Vmalert](help/vmalert.md)

## 5. Настройка Alertmanager

Настраиваем [Alertmanager](help/alertmanager.md)